libsgml-parser-opensp-perl (0.994-7) unstable; urgency=medium

  * Team upload.
  * Add patch from CPAN RT to fix build failure with perl >= 5.37.1
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sun, 02 Jul 2023 21:14:47 +0200

libsgml-parser-opensp-perl (0.994-6) unstable; urgency=medium

  * Team upload.

  [ Helmut Grohne ]
  * Fix FTCBFS: Use a triplet-prefixed compiler. (Closes: #1023487)

 -- gregor herrmann <gregoa@debian.org>  Wed, 23 Nov 2022 21:18:35 +0100

libsgml-parser-opensp-perl (0.994-5) unstable; urgency=medium

  * Fix autopkgtests: the smoke test needs more files.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * debian/rules: fix more permissions of example files.
  * Enable all hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Jun 2022 23:48:03 +0200

libsgml-parser-opensp-perl (0.994-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * Simplify BTS URL.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 10:18:31 +0100

libsgml-parser-opensp-perl (0.994-3) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux)
  * Declare compliance with Debian Policy 3.9.5
  * Forward doc_misspelling.patch

 -- Florian Schlichting <fsfs@debian.org>  Thu, 24 Apr 2014 19:31:19 +0200

libsgml-parser-opensp-perl (0.994-2) unstable; urgency=low

  * Apply patch from Niko Tyni fixing FTBFS in directories with
    characters like '+', such as during binNMUs (Closes: #613199)
  * Update Standards-Version (no changes)

 -- Dominic Hargreaves <dom@earth.li>  Fri, 06 May 2011 19:32:53 +0100

libsgml-parser-opensp-perl (0.994-1) unstable; urgency=low

  * Initial Release. (Closes: #590063)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Mon, 16 Aug 2010 21:18:00 +0100
